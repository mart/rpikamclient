/*
 *   Copyright 2019 Marco Martin <mart@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.5
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.2 as Controls
import org.kde.kirigami 2.7 as Kirigami


Kirigami.ApplicationWindow {
    id: root

    property string url: "http://192.168.1.7/"

    controlsVisible: camera.controlsVisible || pageStack.layers.depth > 1
    pageStack.layers.initialItem: camera

    globalDrawer: Kirigami.GlobalDrawer {
        id: globalDrawer

        Kirigami.Theme.inherit: false
        Kirigami.Theme.colorSet: Kirigami.Theme.View
        leftPadding: 0
        rightPadding: 0
        bottomPadding: 0

        modal: !root.wideScreen || Kirigami.Settings.isMobile
        onModalChanged: {
            if (!modal) {
                open()
            }
        }
        width: Kirigami.Units.gridUnit * 10

        Instantiator {
            id: cameraActions
            model: camerasModel
            property var array: []
            delegate: Kirigami.Action {
                checked: camerasModel.currentCamera.index == index
                text: model.name
                onTriggered: {
                    camerasModel.currentCamera.index = index;
                }
            }
            onObjectAdded: {
                array.splice(index, 0, object)
                globalDrawer.actions = array
            }
            onObjectRemoved: array.splice(index, 1)
        }
        actions: cameraActions.array

        Kirigami.BasicListItem {
            icon: "configure"
            text: "Settings..."
            separatorVisible: false
            checked: pageStack.layers.depth === 2
            onClicked: {
                if (pageStack.layers.depth < 2) {
                    pageStack.initialPage = Qt.resolvedUrl("SettingsPage.qml")
                    pageStack.layers.push(pageStack.contentItem);
                    globalDrawer.visible = !globalDrawer.modal;
                } else {
                    pageStack.layers.pop(pageStack.layers.initialItem);
                }
            }
        }
    }

    CameraPage {
        id: camera
    }

}
