/*
 *   Copyright 2019 Marco Martin <mart@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.10
import QtQuick.Window 2.0
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.3 as Controls
import QtGraphicalEffects 1.0 as Effects

import org.kde.kirigami 2.6 as Kirigami

Controls.Control {
    id: root
    z: 10

    property Kirigami.Page page
    property Item images

    property bool open: false

    state: "hidden"

    background: MouseArea {
        drag.target: root

        ShaderEffectSource {
            id: effectSource
            live: true
            visible: false
            property real animFactor
            sourceItem: images
            anchors.fill: parent
            sourceRect: Qt.rect(root.x + root.width/2*animFactor,
                                root.y + root.height/2*animFactor,
                                root.width - root.width*animFactor,
                                root.height - root.height*animFactor)
        }
        Effects.FastBlur {
            id: blur
            anchors.fill: parent
            source: effectSource
            radius: 32
            
            //scale: 1/root.scale
            visible: false
        }
        Rectangle {
            id: blurMask
            anchors.fill: parent
            radius: width
            visible: false
        }
        Effects.OpacityMask {
            anchors.fill: parent
            source: blur
            maskSource: blurMask
        }
        
        Item {
            anchors {
                fill: parent
                margins: -Kirigami.Units.gridUnit
            }
            opacity: 0.6

            Rectangle {
                id: background
                anchors {
                    fill: parent
                    margins: Kirigami.Units.gridUnit
                }
                radius: width
                color: Kirigami.Theme.backgroundColor
            }
            layer.enabled: true
            Effects.DropShadow {
                anchors.fill: background
                source: background
                horizontalOffset: 0
                transparentBorder: true
                verticalOffset: 1
                radius: Kirigami.Units.gridUnit
                samples: 32
                color: Qt.rgba(0, 0, 0, 0.5)
            }
        }
    }

    width: lay.implicitWidth + leftPadding + rightPadding
    height: lay.implicitHeight + topPadding + bottomPadding

    contentItem: Grid {
        id: lay
        rows: 3
        columns: 3

        Item {width: 1; height: 1}
        Controls.RoundButton {
            icon.name: "go-up"
            onClicked: request(page.url + "pipan.php?action=up")
            background.opacity: pressed
            flat: true
        }
        Item {width: 1; height: 1}

        Controls.RoundButton {
            icon.name: "go-previous"
            onClicked: request(page.url + "pipan.php?action=left")
            background.opacity: pressed
            flat: true
        }
        Item {width: 1; height: 1}
        Controls.RoundButton {
            icon.name: "go-next"
            onClicked: request(page.url + "pipan.php?action=right")
            background.opacity: pressed
            flat: true
        }

        Item {width: 1; height: 1}
        Controls.RoundButton {
            icon.name: "go-down"
            onClicked: request(page.url + "pipan.php?action=down")
            background.opacity: pressed
            flat: true
        }
        Item {width: 1; height: 1}
    }

    states: [
        State {
            name: "shown"
            when: root.open
            PropertyChanges {
                target: root
                opacity: 1
                scale: 1
            }
            PropertyChanges {
                target: effectSource
                animFactor: 0
            }
        },
        State {
            name: "hidden"
            when: !root.open
            PropertyChanges {
                target: root
                opacity: 0
                scale: 0
            }
            PropertyChanges {
                target: effectSource
                animFactor: 1
            }
        }
    ]
    transitions: [
        Transition {
            ParallelAnimation {
                OpacityAnimator {
                    duration: Kirigami.Units.longDuration
                    easing.type: Easing.InOutQuad
                }
                ScaleAnimator {
                    duration: Kirigami.Units.longDuration
                    easing.type: Easing.InOutQuad
                }
                NumberAnimation {
                    target: effectSource
                    property: "animFactor"
                    duration: Kirigami.Units.longDuration
                    easing.type: Easing.InOutQuad
                }
            }
        }
    ]
}

