/*
 *   Copyright 2019 Marco Martin <mart@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.5
import QtQuick.Window 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.2 as Controls

import org.kde.kirigami 2.6 as Kirigami


Kirigami.ScrollablePage {
    id: root

    title: "Settings"
 Kirigami.Theme.inherit: false
    Kirigami.Theme.colorSet: Kirigami.Theme.Window
    actions.main: Kirigami.Action {
        icon.name: "list-add"
        text: i18n("New Camera")
        onTriggered: camerasModel.addCamera("Camera "+view.count, "")
    }

    Kirigami.CardsListView {
        id: view
        model: camerasModel
        property bool removeSlideRight
        delegate: Kirigami.AbstractCard {
            id: card
            width: parent.width
            contentItem: MouseArea {
                drag {
                    target: card
                    axis: Drag.XAxis
                }
                implicitHeight: form.implicitHeight
                onReleased: {
                    if ((card.x < 0 && card.x < -card.width/2) ||
                        (card.x >= 0 && card.x > card.width/2)) {
                        view.removeSlideRight = card.x > 0;
                        camerasModel.removeCamera(index);
                    } else {
                        removeAnim.restart();
                    }
                }
                Controls.ToolButton {
                    parent: card
                    anchors {
                        right: parent.right
                        bottom: parent.bottom
                    }
                    visible: !Kirigami.Settings.isMobile
                    icon.name: "entry-delete"
                    onClicked: camerasModel.removeCamera(index);
                }
                Kirigami.FormLayout {
                    id: form
                    width: parent.width

                    Controls.TextField {
                        id: nameField
                        Layout.fillWidth: true
                        text: model.name
                        Kirigami.FormData.label: "Name:"
                        onTextChanged: camerasModel.configureCamera(index, text, urlField.text);
                    }
                    Controls.TextField {
                        id: urlField
                        Layout.fillWidth: true
                        text: model.url
                        Kirigami.FormData.label: "Url:"
                        onTextChanged: camerasModel.configureCamera(index, nameField.text, text);
                    }
                }
            }
            XAnimator {
                id: removeAnim
                target: card
                duration: Kirigami.Units.longDuration
                from: card.x
                to: 0
                easing.type: Easing.InOutQuad
            }
        }
        remove: Transition {
            NumberAnimation {
                property: "x"
                to: view.removeSlideRight ? view.width : -view.width
                duration: Kirigami.Units.longDuration
                easing.type: Easing.InOutQuad
            }
        }
        removeDisplaced: Transition {
            NumberAnimation {
                property: "y"
                duration: Kirigami.Units.longDuration
                easing.type: Easing.InOutQuad
            }
        }
    }
}
