/*
 *   Copyright 2019 Marco Martin <mart@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.5
import QtQuick.Window 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.2 as Controls
import QtGraphicalEffects 1.0 as Effects

import org.kde.kirigami 2.6 as Kirigami


Kirigami.Page {
    id: root

    title: name
    Kirigami.Theme.inherit: false
    Kirigami.Theme.colorSet: Kirigami.Theme.Window

    property string name: camerasModel.currentCamera.name
    property string url: camerasModel.currentCamera.url
    property alias controlsVisible: controls.open

    globalToolBarStyle: Kirigami.ApplicationHeaderStyle.None

    leftPadding: 0
    topPadding: 0
    rightPadding: 0
    bottomPadding: 0

    function request(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = (function(myxhr) {
            return function() {
                if (callback) {
                    callback(myxhr);
                }
            }
        })(xhr);
        xhr.open('GET', url, true);
        xhr.send('');
    }

    property Image image: image1

    Rectangle {
        id: images
        anchors.fill: parent
        color: "black"
        
        Image {
            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            height: width * (sourceSize.width / sourceSize.height)
            source: "background.svg"
        }
        Image {
            id: image1
            z: image.status == Image.Ready
            asynchronous: true
            anchors.fill: parent
            source: root.url
            onStatusChanged: swapTimer.restart();
            fillMode: Image.PreserveAspectFit
        }
        Image {
            id: image2
            z: image.status == Image.Ready
            asynchronous: true
            anchors.fill: parent
            source: root.url
            onStatusChanged: swapTimer.restart();
            fillMode: Image.PreserveAspectFit
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            if (!controls.open) {
                controls.x = Math.min(width - controls.width - Kirigami.Units.largeSpacing, Math.max(Kirigami.Units.largeSpacing, (mouse.x - controls.width/2)));
                controls.y = Math.min(height - controls.height - Kirigami.Units.largeSpacing, Math.max(Kirigami.Units.largeSpacing, (mouse.y - controls.height/2)));
            }
            controls.open = !controls.open
        }
    }

    CameraControls {
        id: controls
        page: root
        images: images
    }

    Effects.DropShadow {
        z: controls.z+1
        anchors.fill: title
        source: title
        opacity: controls.open
        horizontalOffset: 0
        transparentBorder: true
        verticalOffset: 1
        radius: Kirigami.Units.gridUnit/2
        samples: 32
        color: Qt.rgba(0, 0, 0, 0.8)
        Behavior on opacity {
            OpacityAnimator {
                duration: Kirigami.Units.longDuration
                easing.type: Easing.InOutQuad
            }
        }
    }
    Kirigami.Heading {
        id: title
        anchors {
            left: parent.left
            top: parent.top
            margins: Kirigami.Units.largeSpacing
        }
        visible: false
        text: root.name
        color: "white"
    }

    Timer {
        id: swapTimer
        interval: 25
        onTriggered: {
            if (image == image2) {
                image1.source = root.url + "cam_pic.php?time=" + new Date().getTime();
                image = image1;
            } else {
                image2.source = root.url + "cam_pic.php?time=" + new Date().getTime();
                image = image2;
            }
        }
    }

}
