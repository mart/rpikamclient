/*
 *   Copyright 2016 Marco Martin <mart@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "camerasmodel.h"

#include <QCoreApplication>
#include <QDebug>
#include <QByteArray>

#include <KConfigGroup>


CurrentCamera::CurrentCamera(CamerasModel *parent)
    : QObject(parent),
      m_model(parent)
{
}

CurrentCamera::~CurrentCamera()
{}

int CurrentCamera::index() const
{
    return m_index;
}

void CurrentCamera::setIndex(int index)
{
    if (m_index == index || index < 0 || index > m_model->rowCount()) {
        return;
    }

    m_index = index;

    emit nameChanged();
    emit urlChanged();
    emit indexChanged();
}

QString CurrentCamera::name() const
{
    return m_model->data(m_model->index(m_index), CamerasModel::NameRole).toString();
}

QString CurrentCamera::url() const
{
    return m_model->data(m_model->index(m_index), CamerasModel::UrlRole).toString();
}

/////////////////////////

CamerasModel::CamerasModel(QObject *parent)
    : QAbstractListModel(parent)
{
    m_roleNames.insert(NameRole, "name");
    m_roleNames.insert(UrlRole, "url");

    m_configSyncTimer = new QTimer(this);
    m_configSyncTimer->setSingleShot(false);
    m_configSyncTimer->setInterval(10000);
    connect(m_configSyncTimer, &QTimer::timeout,
        this, [this]() {
            config()->sync();
        });

    beginResetModel();

    for (const QString &groupName : config()->groupList()) {
        if (!groupName.startsWith(QStringLiteral("Camera"))) {
            continue;
        }
        KConfigGroup group(config(), groupName);
        QHash<int, QString> cameraItem({{NameRole, group.readEntry(QStringLiteral("Name"))}, {UrlRole, group.readEntry(QStringLiteral("Url"))}});
        m_cameras << cameraItem;
    }

    endResetModel();

    m_currentCamera = new CurrentCamera(this);
    KConfigGroup cg(config(), QStringLiteral("General"));
    m_currentCamera->setIndex(cg.readEntry("currentCamera", 0));
    connect(m_currentCamera, &CurrentCamera::indexChanged, this, [this]() {
        KConfigGroup cg(config(), QStringLiteral("General"));
        cg.writeEntry("currentCamera", m_currentCamera->index());
        m_configSyncTimer->start();
    });
}

CamerasModel::~CamerasModel()
{
    config()->sync();
}

CurrentCamera *CamerasModel::currentCamera() const
{
    return m_currentCamera;
}

KSharedConfigPtr CamerasModel::config()
{
    if (!m_config) {
        m_config = KSharedConfig::openConfig("rpikamrc", KConfig::SimpleConfig);
    }

    return m_config;
}

void CamerasModel::addCamera(const QString &name, const QString &url)
{
    KConfigGroup cg(config(), QStringLiteral("Camera-%1").arg(m_cameras.size()));
    cg.writeEntry(QStringLiteral("Name"), name);
    cg.writeEntry(QStringLiteral("Url"), url);
    m_configSyncTimer->start();

    beginInsertRows(QModelIndex(), m_cameras.count(), m_cameras.count());
    QHash<int, QString> cameraItem({{NameRole, name}, {UrlRole, url}});
    m_cameras << cameraItem;
    endInsertRows();
}

void CamerasModel::configureCamera(int row, const QString &name, const QString &url)
{
    KConfigGroup cg(config(), QStringLiteral("Camera-%1").arg(row));
    cg.writeEntry(QStringLiteral("Name"), name);
    cg.writeEntry(QStringLiteral("Url"), url);
    m_configSyncTimer->start();

    m_cameras[row][NameRole] = name;
    m_cameras[row][UrlRole] = url;

    emit dataChanged(index(row, 0), index(row, 0), {NameRole, UrlRole});
}

void CamerasModel::removeCamera(int row)
{
    beginRemoveRows(QModelIndex(), row, row);
    config()->deleteGroup(QStringLiteral("Camera-%1").arg(row));
    m_cameras.removeAt(row);
    m_configSyncTimer->start();
    endRemoveRows();
}

QHash<int, QByteArray> CamerasModel::roleNames() const
{
    return m_roleNames;
}

int CamerasModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_cameras.size();
}

QVariant CamerasModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.row() < 0 || index.row() > m_cameras.size()) {
        return QVariant();
    }

    QHash<int, QString> data = m_cameras.value(index.row());

    switch (role) {
    case NameRole:
        return data.value(NameRole);
    case UrlRole:
        return data.value(UrlRole);
    }

    return QVariant();
}


#include "moc_camerasmodel.cpp"
