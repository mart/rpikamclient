/*
 *   Copyright 2019 Marco Martin <mart@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "camerasmodel.h"

int main(int argc, char *argv[])
{
    //QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);

    QStringList arguments;
    for (int a = 0; a < argc; ++a) {
        arguments << QString::fromLocal8Bit(argv[a]);
    }
    QCommandLineParser parser;

    auto urlOption = QCommandLineOption(QStringLiteral("url"), QStringLiteral("url"), QStringLiteral("url"));

    parser.addOption(urlOption);
    parser.addHelpOption();
    parser.process(arguments);

    QString url = parser.value(urlOption);

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty(QLatin1String("camerasModel"), new CamerasModel);
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (!url.isEmpty()) {
        engine.rootObjects().first()->setProperty("url", url);
    }


    return app.exec();
}
