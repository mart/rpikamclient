/*
 *   Copyright 2019 Marco Martin <mart@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#pragma once

#include <QAbstractListModel>
#include <QTimer>
#include <ksharedconfig.h>

class CamerasModel;

class CurrentCamera : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int index READ index WRITE setIndex NOTIFY indexChanged)
    Q_PROPERTY(QString name READ name NOTIFY nameChanged)
    Q_PROPERTY(QString url READ url NOTIFY urlChanged)

public:
    explicit CurrentCamera(CamerasModel *parent);
    ~CurrentCamera();

    int index() const;
    void setIndex(int index);

    QString name() const;
    QString url() const;

Q_SIGNALS:
    void indexChanged();
    void nameChanged();
    void urlChanged();

private:
    CamerasModel *m_model;
    int m_index = -1;
};

class CamerasModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(CurrentCamera *currentCamera READ currentCamera CONSTANT)

public:
    enum Roles {
        NameRole = Qt::UserRole,
        UrlRole
    };

    explicit CamerasModel(QObject *parent = 0);
    ~CamerasModel();

    CurrentCamera *currentCamera() const;

    KSharedConfigPtr config();

    Q_INVOKABLE void addCamera(const QString &name, const QString &url);
    Q_INVOKABLE void configureCamera(int row, const QString &name, const QString &url);
    Q_INVOKABLE void removeCamera(int row);

    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;

private:
    QHash<int, QByteArray> m_roleNames;
    QTimer *m_configSyncTimer;
    CurrentCamera *m_currentCamera;

    KSharedConfigPtr m_config;
    QList<QHash<int, QString> > m_cameras;
};

